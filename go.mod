module bitbucket.org/modima/dbsync2

go 1.13

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20191124224453-732737034ffd
	github.com/go-sql-driver/mysql v1.4.1
	github.com/knadh/koanf v0.6.0
	github.com/lib/pq v1.2.0
	github.com/spf13/pflag v1.0.3
	google.golang.org/appengine v1.6.5 // indirect
)
