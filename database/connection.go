package database

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"
	"math"
	"math/rand"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

type DBConnection struct {
	DB          *sql.DB
	DBType      string
	TablePrefix string
}

// use []map[string]string to preserve order (golang maps are unordered)
var tableSchemas = map[string][]map[string]string{
	"contact": {
		{"$id": "short_string"},
		{"$ref": "short_string"},
		{"$version": "short_string"},
		{"$campaign_id": "short_string"},
		{"$task_id": "short_string"},
		{"$task": "short_string"},
		{"$status": "short_string"},
		{"$status_detail": "short_string"},
		{"$created_date": "short_string"},
		{"$entry_date": "short_string"},
		{"$owner": "short_string"},
		{"$follow_up_date": "short_string"},
		{"$phone": "short_string"},
		{"$timezone": "short_string"},
		{"$caller_id": "short_string"},
		{"$source": "short_string"},
		{"$comment": "text"},
		{"$error": "short_string"},
		{"$recording": "string"},
		{"$recording_url": "string"},
	},
	"transaction": {
		{"id": "short_string"},
		{"contact_id": "short_string"},
		{"task_id": "short_string"},
		{"task": "short_string"},
		{"status": "short_string"},
		{"status_detail": "short_string"},
		{"fired": "short_string"},
		{"pause_time_sec": "int"},
		{"edit_time_sec": "int"},
		{"wrapup_time_sec": "int"},
		{"user": "short_string"},
		{"user_loginName": "short_string"},
		{"user_branch": "short_string"},
		{"user_tenantAlias": "short_string"},
		{"actor": "short_string"},
		{"type": "short_string"},
		{"result": "short_string"},
		{"trigger": "short_string"},
		{"isHI": "bool"},
		{"revoked": "bool"},
	},
	"connection": {
		{"id": "short_string"},
		{"contact_id": "short_string"},
		{"transaction_id": "short_string"},
		{"started": "short_string"},
		{"connected": "short_string"},
		{"disconnected": "short_string"},
		{"duration": "int"},
		{"remote_number": "short_string"},
		{"line_number": "short_string"},
		{"technology": "short_string"},
		{"result": "short_string"},
		{"code": "int"},
	},
	"recording": {
		{"id": "short_string"},
		{"contact_id": "short_string"},
		{"connection_id": "short_string"},
		{"started": "short_string"},
		{"stopped": "short_string"},
		{"filename": "string"},
		{"location": "string"},
	},
	"inbound_call": {
		{"id": "short_string"},
		{"line_id": "short_string"},
		{"campaign_id": "short_string"},
		{"task_id": "short_string"},
		{"contact_id": "short_string"},
		{"remote_number": "short_string"},
		{"line_number": "short_string"},
		{"started": "short_string"},
		{"connected": "short_string"},
		{"disconnected": "short_string"},
		{"user": "short_string"},
	},
}

var dbTypes = map[string]map[string]string{
	"mysql": {
		"short_string":            "varchar(50)",
		"string":                  "text",
		"text":                    "text",
		"int":                     "numeric",
		"float64":                 "numeric",
		"json.Number":             "numeric",
		"bool":                    "boolean",
		"map[string]interface {}": "json",
		"[]interface {}":          "json",
	},
	"postgres": {
		"short_string":            "varchar(50)",
		"string":                  "varchar(255)",
		"text":                    "text",
		"int":                     "numeric",
		"float64":                 "numeric",
		"json.Number":             "numeric",
		"bool":                    "boolean",
		"map[string]interface {}": "json",
		"[]interface {}":          "json",
	},
	"sqlserver": {
		"short_string":            "nvarchar(50)",
		"string":                  "nvarchar(255)",
		"text":                    "text",
		"int":                     "numeric",
		"float64":                 "numeric",
		"json.Number":             "numeric",
		"bool":                    "bit",
		"map[string]interface {}": "nvarchar(4000)", // Maximum is 4000
		"[]interface {}":          "nvarchar(4000)", // Maximum is 4000
	},
}

func (con *DBConnection) toDBType(gotype string) string {

	var dbType = dbTypes[con.DBType][gotype]
	if dbType == "" {
		panic("unsupported type " + gotype)
	}

	return dbType
}

var errorLog *log.Logger
var debugLog *log.Logger

// URIs:
// MYSQL: user:password@/{TABLE_PREFIX}ml_camp?charset=utf8mb4&collation=utf8mb4_bin
// POSTGRES: postgres://user:password@localhost:5432/{TABLE_PREFIX}ml_camp
// MSSQL: sqlserver://user:password@localhost:1433?database={TABLE_PREFIX}ml_camp
func Open(dbType string, uri string, tablePrefix string, dl *log.Logger, el *log.Logger) (*DBConnection, error) {

	debugLog = dl
	errorLog = el

	var db *sql.DB
	var err error

	switch dbType {

	case "mysql":
		// Remove protocol prefix (not allowed with mysql)
		var idx1 = strings.Index(uri, "://")
		var idx2 = strings.Index(uri, "@")
		var idx3 = strings.LastIndex(uri, "/")
		uri = uri[idx1+3:idx2+1] + "tcp(" + uri[idx2+1:idx3] + ")" + uri[idx3:]
		uri += "?charset=utf8mb4&collation=utf8mb4_general_ci"
		debugLog.Printf("Connect to mysql: %v\n", uri)
		db, err = sql.Open("mysql", uri)

	case "sqlserver":
		var dbIdx = strings.LastIndex(uri, "/")
		uri = uri[:dbIdx] + "?database=" + uri[dbIdx+1:]
		debugLog.Printf("Connect to sqlserver: %v\n", uri)
		db, err = sql.Open("mssql", uri)

	case "postgres":
		debugLog.Printf("Connect to postgres: %v\n", uri)
		db, err = sql.Open("postgres", uri)
	}

	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	var con = DBConnection{
		DB:          db,
		DBType:      dbType,
		TablePrefix: tablePrefix,
	}

	return &con, nil
}

type Entity struct {
	Type string
	Data map[string]interface{}
}

func (e *Entity) GetPrimaryKeyName() string {
	if e.Type == "contact" {
		return "$id"
	} else {
		return "id"
	}
}

func (e *Entity) GetID() string {
	if e.Type == "contact" {
		return e.Data["$id"].(string)
	} else {
		return e.Data["id"].(string)
	}
}

func jitter(millis int) int {
	return millis/2 + rand.Intn(millis)
}

func Contains(a []map[string]string, x string) bool {
	for i := 0; i < len(a); i++ {
		for key := range a[i] {
			if key == x {
				return true
			}
		}
	}
	return false
}

/*
var (
	cntContacts     = 0
	cntTransactions = 0
	cntConnections  = 0
	cntRecordings   = 0
)
*/

func (con *DBConnection) Upsert(entity Entity) error {

	var tableName = con.TablePrefix + entity.Type + "s"
	var data = filter(entity)

	// Extract fieldNames and values
	var fieldNames []string
	var values []interface{}
	for name, value := range data {

		switch entity.Type {

		case "contact":
			if len(value.(string)) == 0 {
				continue // Skip empty values (all string in contacts)
			}

		default:
			if !Contains(tableSchemas[entity.Type], name) {
				continue // Skip fields that are not defined in schema definition
			}
		}

		fieldNames = append(fieldNames, name)
		values = append(values, con.toDBString(value))
	}

	// Prepare statement
	var (
		stmt   *sql.Stmt
		result sql.Result
		err    error
	)
	for i := 0; i < 10; i++ {
		stmt, err = con.PrepareUpsertStatement(tableName, fieldNames)
		if err == nil {
			break
		}
		timeout := time.Second*time.Duration(math.Pow(2, float64(i))) + time.Duration(jitter(5000))*time.Millisecond
		debugLog.Printf("prepare upsert %v %v failed with error: %v | retry in %v", entity.Type, entity.GetID(), err.Error(), timeout)
		time.Sleep(timeout)
	}

	// Daten duplizieren (1. Insert / 2. Update)
	values = append(values, values...)

	// SQLServer benötigt zusäzliches $id Feld für Query
	if con.DBType == "sqlserver" {
		values = append([]interface{}{entity.GetID()}, values...)
	}

	// Execute statement
	for i := 0; i < 10; i++ {
		result, err = stmt.Exec(values...)
		if err == nil {
			cnt, err := result.RowsAffected()
			if err != nil {
				debugLog.Printf("upsert %v %v failed with error: %v", entity.Type, entity.GetID(), err.Error())
			} else if cnt != 1 {
				debugLog.Printf("upsert %v %v failed: %v rows inserted | contact: %v", entity.Type, entity.GetID(), cnt, entity.Data["contact_id"])
			}

			break
		}
		timeout := time.Second*time.Duration(math.Pow(2, float64(i))) + time.Duration(jitter(5000))*time.Millisecond
		debugLog.Printf("upsert %v %v failed with error: %v | retry in %v", entity.Type, entity.GetID(), err.Error(), timeout)
		time.Sleep(timeout)
	}

	// Close statement
	stmt.Close()

	// DEBUGGING START
	/*
		switch entity.Type {
		case "contact":
			cntContacts++
			if cntContacts > 35000 {
				debugLog.Printf("contacts inserted: %v | last: %v", cntContacts, entity.GetID())
			}
		case "transaction":
			cntTransactions++
			if cntTransactions > 465000 {
				debugLog.Printf("transactions inserted: %v | last: %v", cntTransactions, entity.GetID())
			}
		case "connection":
			cntConnections++
			if cntConnections > 385000 {
				debugLog.Printf("connections inserted: %v | last: %v", cntConnections, entity.GetID())
			}
		case "recording":
			cntRecordings++
			if cntRecordings > 24000 {
				debugLog.Printf("recordings inserted: %v | last: %v", cntRecordings, entity.GetID())
			}
		default:
			errorLog.Printf("insert of unknown type: %v", entity.Type)
		}
	*/
	// DEBUGGING END

	return err
}
func (con *DBConnection) PrepareUpsertStatement(tableName string, data []string) (*sql.Stmt, error) {

	var b bytes.Buffer

	switch con.DBType {

	case "mysql":
		con.PrepareUpsertMySQL(tableName, data, &b)

	case "postgres":
		con.PrepareUpsertPostgres(tableName, data, &b)

	case "sqlserver":
		con.PrepareUpsertSQLServer(tableName, data, &b)
	}

	//debugLog.Printf("%v", b.String())
	return con.DB.Prepare(b.String())
}

func filter(entity Entity) map[string]interface{} {

	var filteredData = make(map[string]interface{})

	for _, col := range tableSchemas[entity.Type] {
		for cName := range col {
			if (entity.Data)[cName] != nil {
				filteredData[cName] = (entity.Data)[cName]
			}
		}
	}

	return filteredData
}

type Campaign struct {
	Form struct {
		Elements []struct {
			Type      string `json:"type"`
			FieldType string `json:"fieldType"`
			Name      string `json:"name"`
			State     string `json:"state"`
			Deleted   bool   `json:"deleted"`
		} `json:"elements"`
	} `json:"form"`
}

func (con *DBConnection) UpdateTables(campaign Campaign) error {

	// Maximal 100 weitere Spalten
	var count = 0
	for _, element := range campaign.Form.Elements {

		if element.Type != "field" || element.Deleted == true {
			continue
		}

		/* Kampagnentypen:
		"text":         "string",
		"date":         "string",
		"calendar":     "string",
		"phone":        "string",
		"radiogroup":   "string",
		"dropdown":     "string",
		"autocomplete": "string",
		"checkbox":     "bool",
		"number":       "int", // TODO: FlieÃŸkomma unterstÃ¼tzen
		"textarea":     "text",
		*/
		// Abbildung auf datenbanktype
		var dbType string
		switch element.FieldType {

		/* Datatype of checkbox is String
		case "checkbox":
			dbType = "bool"
		*/

		case "number":
			dbType = "int"

		case "textarea":
			dbType = "text"
			break

		default:
			dbType = "string"
		}

		tableSchemas["contact"] = append(tableSchemas["contact"], map[string]string{element.Name: dbType})
		count++

		// Maximal 200 Elemente
		if count == 200 {
			break
		}
	}

	// ggf. Tabellen erzeugen
	for entityType, columns := range tableSchemas {
		if err := con.createTable(entityType, columns); err != nil {
			return err
		}
		if err := con.updateColumns(entityType, columns); err != nil {
			return err
		}
	}

	return nil
}

func (con *DBConnection) createTable(entityType string, columns []map[string]string) error {

	var tableName = con.TablePrefix + entityType + "s"

	var b bytes.Buffer

	switch con.DBType {

	case "mysql":
		con.CreateTableMySQL(tableName, columns, &b)

	case "postgres":
		con.CreateTablePostgres(tableName, columns, &b)

	case "sqlserver":
		con.CreateTableSQLServer(tableName, columns, &b)
	}

	//debugLog.Printf("%v\n\n", b.String())
	_, err := con.DB.Exec(b.String())
	if err != nil {
		errorLog.Printf("%v\n", b.String())
		errorLog.Printf("%v \n", err.Error())
	}
	return err
}

func (con *DBConnection) updateColumns(entityType string, columns []map[string]string) error {

	// ggf. Tabelle aktualisieren
	var newColumns = make(map[string]string)
	var existingColumns = con.getTableColumns(entityType)
	for _, col := range columns {
		for cName, cType := range col {
			if existingColumns[cName] == "" {
				newColumns[cName] = cType
			}
		}
	}

	// Datenbankschema aktualisieren
	if len(newColumns) > 0 {
		if err := con.addTableColumns(entityType, newColumns); err != nil {
			return err
		}
	}
	return nil
}

func (con *DBConnection) toDBString(value interface{}) string {

	var result string

	switch value.(type) {

	case json.Number:
		if strings.Contains(string(value.(json.Number)), ".") {
			vNum, _ := value.(json.Number).Float64()
			result = strconv.FormatFloat(vNum, 'f', 10, 64)
		} else {
			vNum, _ := value.(json.Number).Int64()
			result = strconv.FormatInt(vNum, 10)
		}

	case []interface{}:
		jsonBytes, err := json.Marshal(value)
		if err != nil {
			panic(err)
		}

		//result = "'" + string(jsonBytes) + "'"
		result = string(jsonBytes)

	case map[string]interface{}:
		jsonBytes, err := json.Marshal(value)
		if err != nil {
			panic(err)
		}

		//result = "'" + string(jsonBytes) + "'"
		result = string(jsonBytes)

	case int:
		result = strconv.Itoa(value.(int))

	case bool:
		if value.(bool) {
			result = strconv.FormatInt(1, 10)
		} else {
			result = strconv.FormatInt(0, 10)
		}

	case string:
		var runes = []rune(value.(string))

		// Auf 255 Zeichen beschränken
		if len(runes) > 255 {
			runes = runes[:255]
		}

		result = string(runes)
		if !utf8.ValidString(result) {
			debugLog.Printf("convert invalid string: %v", result)
			//see: https://stackoverflow.com/questions/20401873/remove-invalid-utf-8-characters-from-a-string
			v := make([]rune, 0, len(result))
			for i, r := range result {
				if r == utf8.RuneError {
					_, size := utf8.DecodeRuneInString(result[i:])
					if size == 1 {
						continue
					}
				}
				v = append(v, r)
			}
			result = string(v)
		}

	default:
		panic("unsupported type " + reflect.TypeOf(value).String())
	}

	// Escape special characters
	//result = strings.Replace(result, "'", "\\'", -1)
	//result = strings.Replace(result, "\\\\'", "\\'", -1)

	//return "'" + result + "'"x
	//debugLog.Printf("insert value: %v\n", result)

	return result
}

func (con *DBConnection) getTableColumns(entityType string) map[string]string {

	var tableName = con.TablePrefix + entityType + "s"

	var stmt = ""

	switch con.DBType {

	case "mysql":
		stmt = "SELECT column_name FROM information_schema.columns WHERE table_name = '" + tableName + "';"

	case "sqlserver":
		stmt = "SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('" + tableName + "')"

	case "postgres":
		stmt = "SELECT column_name FROM information_schema.columns WHERE table_name = '" + tableName + "';"
	}

	rows, err := con.DB.Query(stmt)
	if err != nil {
		errorLog.Printf("%v\n", stmt)
		errorLog.Printf("%v \n", err.Error())
		os.Exit(1)
	}

	defer rows.Close()

	var columns = map[string]string{}
	var col string
	for rows.Next() {
		rows.Scan(&col)
		columns[col] = col
	}

	return columns
}

func (con *DBConnection) addTableColumns(entityType string, newColumns map[string]string) error {

	var tableName = con.TablePrefix + entityType + "s"

	var b bytes.Buffer

	switch con.DBType {

	case "mysql":
		con.AddColumnsMySQL(tableName, newColumns, &b)

	case "postgres":
		con.AddColumnsPostgres(tableName, newColumns, &b)

	case "sqlserver":
		con.AddColumnsSQLServer(tableName, newColumns, &b)
	}

	//debugLog.Printf("%v\n\n", b.String())
	_, err := con.DB.Exec(b.String())
	if err != nil {
		errorLog.Printf("%v\n", b.String())
		errorLog.Printf("%v \n", err.Error())
	}
	return err
}
